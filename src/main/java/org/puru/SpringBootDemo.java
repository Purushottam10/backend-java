package org.puru;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootDemo  {
    public static  void main(String ... arg){
        SpringApplication.run(SpringBootDemo.class, arg);
    }
}
